module.exports = function customErrorHandler(event, body)
{
    event.response.statusCode = 400;
    event.response.body = 'This is a custom error body';
};