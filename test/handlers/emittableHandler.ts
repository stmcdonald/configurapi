import { IEvent } from '../../src/interfaces/iEvent';
import { LogLevel } from '../../src/interfaces/logLevel';

export async function emittableHandler(event: IEvent)
{
    this.emit(LogLevel.Trace, "Hello from emittableHandler");
};