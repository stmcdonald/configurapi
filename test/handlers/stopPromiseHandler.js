module.exports = async function stopPromiseHandler(event, body)
{
    return new Promise((resolve, reject) => {
        this.complete();
        resolve();
    });
};