import { assert } from 'chai';
import { Event } from '../src/entities/event';
import { createRequest } from './helper';
import { Policy } from '../src/entities/policy';
import { Route } from '../src/entities/route';

describe('Route', function() {
    describe("Process", function()
    {
        it('Process all handlers', async function(){
            let policy1 = new Policy();
            policy1.handler = function(ev)
            {
                ev.response.body += "1";
            };
            
            let policy2 = new Policy();
            policy2.handler = function(ev)
            {
                ev.response.body += "2";
            };

            let ev = new Event(createRequest('GET','/collections'));
            
            let route = new Route();
            route.policies.push(policy1);
            route.policies.push(policy2);

            await route.process(ev);
            
            assert.equal('12', ev.response.body);
        });

        it('Process a handler that terminates with complete()', async function(){
            let policy1 = new Policy();
            policy1.handler = function(ev)
            {
                ev.response.body += "1";
                this.complete();
            };
            
            let policy2 = new Policy();
            policy2.handler = function(ev)
            {
                ev.response.body += "2";
            };

            let ev = new Event(createRequest('GET','/collections'));
            
            let route = new Route();
            route.policies.push(policy1);
            route.policies.push(policy2);

            await route.process(ev);
            
            assert.equal('1', ev.response.body);
        });

        it('Process a handler that terminates with catch()', async function(){
            let policy = new Policy();
            policy.handler = function(ev)
            {
                this.catch("error_message");
            };
            
            let ev = new Event(createRequest('GET','/collections'));
            
            let route = new Route();
            route.policies.push(policy);

            let error;

            try
            {
                await route.process(ev);
            }
            catch(e)
            {
                error = e;
            }

            assert.isDefined(error, "Expects the route processing to throw an exception.");
            assert.equal('error_message', error);
        });
    });
});