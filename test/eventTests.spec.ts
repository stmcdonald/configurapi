let assert = require('chai').assert;
import { Event } from '../src/entities/event';
let equal = require('deep-equal');
let parse = require('url');
let helper = require('./helper');

describe('Event', function() 
{
	[
        {expected: 'list_resources', method: 'get', path: '/resources', params: {'resource': ''}},
        {expected: 'get_resource', method: 'get', path: '/resources/id', params: {'resource': 'id'}},
        {expected: 'list_resource_subresources', method: 'get', path: '/resources/id/subresources', params: {'resource': 'id', 'subresource': ''}},
        {expected: 'get_resource_subresource', method: 'get', path: '/resources/id1/subresources/id2', params: {'resource': 'id1', 'subresource': 'id2'}},
        {expected: 'post_resource', method: 'post', path: '/resources', params: {'resource': ''}},
        {expected: 'delete_resource', method: 'delete', path: '/resources', params: {'resource': ''}},
        {expected: 'put_resource', method: 'put', path: '/resources', params: {'resource': ''}},
        {expected: 'list_resources', method: 'get', path: '/resources?var1=true&var2=abc&var1=false', params: {'resource': '', 'var1': ['true', 'false'], 'var2': 'abc'}},
        {expected: 'put_v1_resource', method: 'put', path: '/v1/resources', params: {'resource': ''}}      
    ]
    .forEach(function(test) 
    {
        describe(test.expected, function()
        {
            let event = new Event(helper.createRequest(test.method, test.path));

            it("name", function() {
                assert.equal(test.expected, event.name);
            });

            it("param", function() {
                assert.equal(true, equal(test.params, event.params));
            });
        });
    });

    [
        {expected: 3.14, str: '$(pi)'},
        {expected: 'Hello, world', str: 'Hello, $(name)'},
        {expected: '3.14 is equal to 3.14', str: '$(pi) is equal to $(pi)'},
        {expected: 'world is not 3.14', str: '$(name) is not $(pi)'},
        {expected: 'Boolean is true or false', str: 'Boolean is $(true) or $(false)'},
        {expected: 'Non-existent parameter is $(non-existent)', str: 'Non-existent parameter is $(non-existent)'},
        {expected: 'Null is null', str: 'Null is $(null)'},
        {expected: 'Array is 1,2,3', str: 'Array is $(array)'},
        {expected: 'Object is [object Object]', str: 'Object is $(obj)'},
        {expected: 'Object property is 1', str: 'Object property is $(obj.key)'},
        {expected: 'Object sub property is 2', str: 'Object sub property is $(obj.nested.subKey)'},
        {expected: 'Object array property is b', str: 'Object array property is $(obj.list[1])'},
        {expected: 'Object non-existent property is $(obj.non-existent)', str: 'Object non-existent property is $(obj.non-existent)'},
        {expected: 'Invalid format is $(ok.', str: 'Invalid format is $(ok.'},
        {expected: 'No parameter is ok.', str: 'No parameter is ok.'}
    ]
    .forEach(function(test) 
    {
        describe('string ' + test.str, function()
        {
            let event = new Event(helper.createRequest('get', '/'));
            event.params['pi'] = 3.14;
            event.params['name'] = 'world';
            event.params['true'] = true;
            event.params['false'] = false;
            event.params['null'] = null;
            event.params['array'] = [1,2,3];
            event.params['obj'] = { key:1,
                                    list: ['a','b', 'c'],
                                    nested: {
                                        subKey:2
                                    }
                                  };

            it('can be resolved', function()
            {
                assert.equal(event.resolve(test.str), test.expected);
            })
        });
    });

    describe('resolve', function()
    {
        it('environment variables can be resolved.', function()
        {
            let event = new Event(helper.createRequest('get', '/'));
            
            assert.isDefined(event.resolve('$($env.HOME)'));
            assert.equal(event.resolve('$($env.UNKNOWN)'), '$($env.UNKNOWN)');
        });

        it('request string payload can be resolved.', function()
        {
            let event = new Event(helper.createRequest('get', '/'));
            event.payload = "Hi";

            assert.equal(event.resolve('$($event.payload) there'), 'Hi there');
        });
        
        it('request payload can be resolved.', function()
        {
            let event = new Event(helper.createRequest('get', '/'));
            event.payload = { pi: 3.14 };

            assert.equal(event.resolve('Pi is $($event.payload.pi)'), 'Pi is 3.14');
        });

        it('empty request payload will not cause any problem during resolving', function()
        {
            let event = new Event(helper.createRequest('get', '/'));
            event.payload = '';

            assert.equal(event.resolve('Pi is $($event.payload.pi)'), 'Pi is $($event.payload.pi)');
        });

        it('request payload can be resolved (nested)', function()
        {
            let event = new Event(helper.createRequest('get', '/'));
            event.payload = { items: [ {id:1}, {id:2}] };

            assert.equal(event.resolve('Two is $($event.payload.items[1].id)'), 'Two is 2');
        });

        it('object properties can be resolved', function()
        {
            let event = new Event(helper.createRequest('get', '/'));
            event.params['pi'] = '3.14';

            let resolvedObj = event.resolve({ prop1: '$(pi)', 
                                              prop2: 2.14, 
                                              prop3: {prop31: 'Pi=$(pi)'}, 
                                              prop4: ['1', '2', '$(pi)'],
                                              prop5: '$(unknown)'
                                            });

            assert.equal(resolvedObj.prop1, '3.14');
            assert.equal(resolvedObj.prop2, 2.14);
            assert.equal(resolvedObj.prop3.prop31, 'Pi=3.14');
            assert.equal(resolvedObj.prop4[0], '1');
            assert.equal(resolvedObj.prop4[1], '2');
            assert.equal(resolvedObj.prop4[2], '3.14');
            assert.equal(resolvedObj.prop5, '$(unknown)');
        });
    });

    describe('payload', function()
    {
        it('is populated', function()
        {
            let event = new Event(helper.createRequest('get', '/', 'body'));

            assert.equal(event.payload, 'body');
        });
    });

    describe('Event', function()
    {
        it('Id is populated', function()
        {
            let event = new Event(helper.createRequest('get', '/', 'body'));

            assert.equal(event.id.length, 36);
            assert.equal(event.correlationId, event.id);
        });

        it('CorrelationId is populated - query string', function()
        {
            let event = new Event(helper.createRequest('get', '/?correlationid=123', 'body'));
            
            assert.equal(event.correlationId, '123');
        });

        it('CorrelationId is populated - header', function()
        {
            let event = new Event(helper.createRequest('get', '/', 'body', {'correlation-id': '234'}));
            
            assert.equal(event.correlationId, '234');
        });
    }); 
});