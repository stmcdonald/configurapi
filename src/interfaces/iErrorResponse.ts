import { IResponse } from "./iResponse";

export interface IErrorResponse extends IResponse
{
    message: Error | IResponse | string;
    details: string;
}