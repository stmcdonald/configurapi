import { IClaim } from './iClaim';

export interface IIdentity 
{
    id: string;
    issuer: string;
    name: string;
    email: string;
    claims: Partial<IClaim>[];

    hasClaim(type: string, valueOrValues: string | string[], scopeOrScopes?: string | string[]): boolean;
    getClaim(type: string, value: string, scope?: string): Partial<IClaim>;
    getClaims(type: string, value: string, scope?: string): IClaim[];
}