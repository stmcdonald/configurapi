export interface IResponse 
{
    body: any;
    statusCode: number;
    headers: Record<string, string>;
}