import { ParsedUrlQuery } from "querystring";

export interface IRequest 
{
    name: string;
    method: string;
    headers: Record<string, string>;
    params: Record<string, string>;
    payload: string | Record<string, any> | undefined;
    query: ParsedUrlQuery | undefined;
    path: string | undefined;
    pathAndQuery: string;
}