import { IRoute } from "./iRoute"

export interface IYamlApi 
{
    events: Set<IRoute>
}

export interface IYamlConfig 
{
    api: IYamlApi
}