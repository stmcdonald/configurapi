import EventEmitter = require("events");
import { IEvent } from "./iEvent";
import { LogLevel } from "./logLevel";
import { Result } from "./result";

export interface IPolicy extends EventEmitter 
{
    name: string;
    parameters: any[];
    logLevel: LogLevel;
    handler: Function;

    process(event: IEvent): Promise<Result>
}