import { IEvent } from "./iEvent";
import { IServiceListener } from "./iServiceListener";

export interface IService 
{
    on(type:string, listener:IServiceListener): void;
    process(event:IEvent):Promise<void>;
}