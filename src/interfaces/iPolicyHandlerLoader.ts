export type FunctionType = (...args) => any | Function;

export interface IPolicyHandlerLoader 
{
    handlers: Map<String, FunctionType>;
    loadCustomHandlers(handler: string): void;
    load(moduleOrFileName: string): void;
    get(handlerName: string): Function;
}