import { EventEmitter } from 'events';
import { IEvent, Result } from '../configurapi';

import { IPolicy } from '../interfaces/iPolicy';
import { LogLevel } from '../interfaces/logLevel';

export class Policy extends EventEmitter implements IPolicy 
{
    name: string;
    parameters: any[];
    logLevel: LogLevel;
    handler: Function;

    constructor(policy?: Partial<IPolicy>) 
    {
        super();

        this.name = policy?.name || "";
        this.parameters = policy?.parameters || [];
        this.logLevel = policy?.logLevel || LogLevel.Trace;
        this.handler = policy?.handler || undefined;
    }
    
    async process(event: IEvent): Promise<Result>
    {
        let state = Result.Continue;

        let context = {
            'continue': () => state = Result.Continue,
            'complete': () => state = Result.Completed,
            'catch': ( error: Error ) => { throw error },
            'emit': ( level: LogLevel, message: string ) => this.emit(level, `${event.id} - ${event.correlationId} - ${message}`)
        };

        let handlerParams = this.parameters instanceof Array ? this.parameters.slice(0) : [];
        handlerParams.unshift(event);

        try
        {
            await this.handler.apply(context, handlerParams);
            return state;
        }
        catch (error)
        {
            throw error;
        }
    }
}