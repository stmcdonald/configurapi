import { IErrorResponse } from "../interfaces/iErrorResponse";
import { Response } from "./response";

export class ErrorResponse extends Response implements IErrorResponse 
{
    error: Error | ErrorResponse | string;
    message: Error | ErrorResponse | any;
    details: string;

    constructor(message: Error | ErrorResponse | any, statusCode: number = 500, details: string = '')
    {
        super('', statusCode, {'Content-Type': 'application/json'});

        this.error = message;
        this.message = message;
        this.details = details;

        if(message instanceof ErrorResponse)
        {
            let e = message;
            this.message = e.message;
            this.details = e.details;
            this.statusCode = e.statusCode;
        }
        else if(message instanceof Error)
        {
            this.message = message.message;
            this.details = message.stack;
        }
        else
        {
            this.message = message;
            this.details = details;
        }

        this.body = {'statusCode': this.statusCode, 'message': this.message, 'details': this.details};
    }

    static isErrorResponse(obj: any): boolean
    {
        return typeof obj === 'object' && 'statusCode' in obj && 'error' in obj && 'message' in obj && 'details' in obj && 'body' in obj; 
    }
}