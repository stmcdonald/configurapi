import { lstatSync, readdirSync } from 'fs';
import NestedError = require('nested-error-stacks');
import { resolve } from 'path';

import { FunctionType, IPolicyHandlerLoader } from '../interfaces/iPolicyHandlerLoader';

export class PolicyHandlerLoader implements IPolicyHandlerLoader 
{
    constructor(public handlers: Map<String, FunctionType> = new Map<String, FunctionType>()) {}

    get(handlerName: string): Function
    {
        if(this.handlers.has(handlerName))
        {
            return this.handlers.get(handlerName);
        }
        else
        {
            throw new Error(`Handler '${handlerName}' could not be found.`);
        }
    }

    load(moduleOrFileName: string): void
    {
        let exports = this.resolve(moduleOrFileName);

        if(!exports)
        {
            throw new Error(`Cannot find '${moduleOrFileName}'.`);
        }

        this.loadHandler(exports);
    }

    loadCustomHandlers(directory: string): void
    {
        const absoultePath = resolve(process.cwd(), directory);
        try
        {   
            let paths = readdirSync(absoultePath);
            
            for(const path of paths)
            {
                let filePath = `${absoultePath}/${path}`;
                let stat = lstatSync(filePath);
                if(stat.isFile() && path.match(/\.[jt]s$/i) !== null)
                {
                    this.load(absoultePath + "/" + path.replace(/\.[jt]s$/i, ''));
                }
            }
        } catch(error)
        {
            throw new NestedError(`Could not load '${absoultePath}'`, error);
        }
    }

    private resolve(path: string): NodeRequire
    {
        try 
        { 
            return require(path);
        }
        catch(e)
        {
            throw new NestedError(`Failed to load ${path}`, e);
        }
    }

    loadHandler(obj: any): void
    {
        if(typeof obj === "function")
        {
            this.handlers.set(obj.name, obj);
        }
        else
        {
            for(let key in obj)
            {
                if(this.isHandler(obj, key))
                {
                    this.handlers.set(key, obj[key]);
                }
            }
        }
    }

    private isHandler(obj: object, key: string): boolean
    {
        return (typeof obj[key] === "function") && key.match(/handler$/i) !== null;
    }
}