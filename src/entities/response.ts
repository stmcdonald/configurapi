import { IResponse } from '../interfaces/iResponse';

export class Response implements IResponse 
{
    constructor(public body:any = "", public statusCode = 200, public headers:Record<string, string> = {}) 
    {
        this.statusCode = statusCode;
        this.body = body;
        this.headers = headers;
    }
}