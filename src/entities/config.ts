import { readFileSync } from 'fs';
import { load } from 'js-yaml';
import NestedError = require('nested-error-stacks');
import { isAbsolute, parse, resolve } from 'path';

import { IConfig } from '../interfaces/iConfig';
import { IPolicy } from '../interfaces/iPolicy';
import { IRoute } from '../interfaces/iRoute';
import { IYamlApi, IYamlConfig } from '../interfaces/iYamlConfig';
import { LogLevel } from '../interfaces/logLevel';
import { Policy } from './policy';
import { Route } from './route';

export class Config implements IConfig 
{
    constructor(public modules: string[] = [], public events: Map<String, IRoute> = new Map<String, IRoute>()) {}

    static getPathParentDir(path)
    {
        if (!isAbsolute(path))
        {
            path = resolve(process.cwd(), path);
        }
        return parse(path).dir
    }

    static load(path: string): IConfig
    {
        let content = "";

        try
        {
            content = readFileSync(path, 'utf8');
        }
        catch(error)
        {
            throw new NestedError(`Failed to load '${path}'`, error);
        }

        return this.parse(content, this.getPathParentDir(path));
    }

    static parse(content: string, parentDirectory?: string): IConfig
    {
        let document = load(content);
        
        if(document instanceof Object)
        {
            return new Config(this.parseImport(document, parentDirectory), this.parseApi(document));
        }
        else        
        {
            throw new Error("The specified config file is not an invalid yaml.");
        }
    }

    static parseImport(document: object, parentDirectory?: string): string[]
    {
        let result = [];

        //Check for an optional "import"
        if('import' in document)
        {
            if(typeof document['import'][Symbol.iterator] !== 'function')
            {
                throw new Error(`Could not load 'import.${document['import']}' in the config document`);
            }
            
            for(let module of <any> document['import'])
            {
                if (module.startsWith('.'))
                {
                    module = resolve(parentDirectory, module);
                }
                result.push(module);
            }
        } 
        
        return result;
    }

    static parseApi(document: Partial<IYamlConfig>): Map<String, IRoute>
    {
        if('api' in document)
        {
            return this.parseRoutes(document.api);
        }
        else
        {
            throw new Error('Could not find \'api\' in the config document');
        }
    }

    static parseRoutes(api: IYamlApi): Map<String, IRoute>
    {
        if((api instanceof Object) && 'events' in api)
        {
            let result = new Map<String, IRoute>();
            if(typeof api.events[Symbol.iterator] !== 'function')
            {
                throw new Error(`Events could not be empty in the config document.`);
            }

            let routeIndex: number = 0;
            for(let r of api.events)
            {
                let route = this.parseRoute(r, routeIndex);

                if(route.enabled)
                {
                    result.set(route.name, route);
                }
            }

            return result;
        }
        else
        {
            throw new Error('Could not find \'api.events\' in the config document');
        }
    }

    static parseRoute(route: IRoute, routeIndex: number): IRoute
    {
        let result = new Route();
       
        if(route instanceof Object && 'name' in route)
        {
            result.name = route.name;
            result.logLevel = LogLevel[Object.keys(LogLevel).find(key => key.toLowerCase() === (route.logLevel || LogLevel.Trace).toLowerCase())];

            if(route.when)
            {
                if(process.env.NODE_ENV === undefined)
                {
                    result.enabled = false;
                } 
                else
                {
                    result.enabled = process.env.NODE_ENV === route.when;
                }
            }

            result.policies = this.parsePolicies(route, result.name);
            
            return result;
        }
        else
        {
            throw new Error(`Could not find 'api.events[${routeIndex}].name' in the config document`);
        }
    }

    static parsePolicies(route: IRoute, routeName: string): IPolicy[]
    {
        let result = [];
        
        if(route instanceof Object && 'policies' in route)
        {
            let policyIndex: number = 0;

            if(typeof route.policies[Symbol.iterator] !== 'function')
            {
                throw new Error(`Policies 'api.events[${routeName}].policies' could not be empty in the config document.`);
            }

            for(let policy of route.policies)
            {
                result.push(this.parsePolicy(policy, routeName, policyIndex));
            }

            return result;
        }
        else
        {
            throw new Error(`Could not find 'api.events[${routeName}].policies' in the config document`);
        }
    }

    static parsePolicy(policy: IPolicy, routeName: string, policyIndex: number): IPolicy
    {
        let policyName = this.parsePolicyName(policy);
        
        if(!policyName)
        {
            throw new Error(`Could not find 'api.events[${routeName}].policies[${policyIndex}]' in the config document`);
        }
        return new Policy({
            name: policyName,
            parameters: this.parseParameter(policy, policyName),
            logLevel: policy.logLevel || LogLevel.Trace
        });
    }

    static parsePolicyName(policy: IPolicy): string
    {
        if(typeof policy == 'string')
        {
            return policy;
        }
        
        for (var key in policy) {
            if (policy.hasOwnProperty(key)) {
                return key;
            }
            return undefined;
        }
    }

    static parseParameter(policy: IPolicy, policyName: string): any[]
    {
        if (policy[policyName] instanceof Array) 
        {
            return policy[policyName]
        }
        
        return policy.parameters || []
    }
}