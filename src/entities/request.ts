import { ParsedUrlQuery } from "querystring";
import { IRequest } from "../interfaces/iRequest";

export class Request implements IRequest 
{
    name: string;
    method: string;
    headers: Record<string, string>;
    params: Record<string, string>;
    payload: string | Record<string, any> | undefined;
    query: ParsedUrlQuery | undefined;
    path: string | undefined;
    pathAndQuery: string;

    constructor(method) 
    {
        this.name = undefined;
        this.method = (method || "get").toLowerCase();
        this.headers = {};
        this.params = {};
        this.payload = "";
        this.query = {};
        this.path = "";
        this.pathAndQuery = "";
    }
}