# Configurapi

Configurapi allows developers to develop APIs easily, without worrying about the majority of the underlying details.

**Configurapi requires a folder /handlers witin the project which contains the handlers referenced in the config.yaml.**

# Example Config

This is an example of a basic API config file.
An API can have events and modules, events refer to http request name and are formatted by '{http-request-method}_path.
The available methods for the events are list, get, patch, post, and delete. 

Every name refers to an http request type and path, and every policy refers to an exported handler.
Policies also allow parameters to be passed in, which will be passed into the handler when executed.

Modules allow imported handlers from other libraries, for example if handler is created within a library and exported, it can be referenced in an event policies.

<pre>
api:
    modules:
        - some-library-1
        - some-library-2
    events:
        - name: list_collections
          policies:
            - setResponseBodyHandler:
                - "Hi there"
            - setResponseStatusCodeHandler:
                - 201
        - name: post_collection
          policies:
            - setResponseBodyHandler:
                - "This is an expected response."
            - stopPromiseHandler
            - setResponseBodyHandler:
                - "This is an invalid response."
        - name: delete_collection
          policies:
            - setResponseBodyHandler:
                - "This is an expected response."
            - stopHandler
            - setResponseBodyHandler:
                - "This is an invalid response."
        - name: get_collection
          policies:
            - throwErrorHandler
        - name: list_emittable
          policies:
            - emittableHandler
</pre>

#Handler Example
This is a basic handler example.

In this example there is a param which can be passed, this would be passed in the policy for example with config:

<pre>
api:
    events:
        policies:
            - handlerThatDoesSomething:
                - "a passed param"
</pre>

<pre>
import { IEvent } from 'configurapi';
import { JsonResponse } from 'configurapi-handler-json';

export async function handlerThatDoesSomething(event: IEvent, aParam?: string) {
    //...Do Some Stuff
    event.response = new JsonResponse({ param: aParam || "default param if there was none passed" });
    return this.continue();
}
</pre>

Events have the fields correlationId, params, id, query, headers, body, request, payload, identity and response.

If this.complete() is called inside of a handler, it will return at that handler, without going to the next.

this.continue() called inside of a handler will go to the next handler.

Identity allows handlers to store user identity credentials, and can be passed down to preceding handlers.